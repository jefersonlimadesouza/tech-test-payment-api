namespace tech_test_payment_api.Entities
{
    public class ListaProdutos
    {
        public int Id { get; set; }
        public int IdListaProdutos { get; set; }
        public int IdProduto { get; set; }
    }
}