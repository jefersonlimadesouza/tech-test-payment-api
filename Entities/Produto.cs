namespace tech_test_payment_api.Entities
{
    public class Produto
    {
        public int Id { get; set; }
        public string NomeProduto { get; set; }
        public string DescricaoProduto { get; set; }
        public double Preco { get; set; }
        public int Saldo { get; set; }
  }
}