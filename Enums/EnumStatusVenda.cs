using System.EnumAttributes;

namespace tech_test_payment_api.Enums
{
    public enum EnumStatusVenda
    {
        [StringValue("Aguardando pagamento")]AguardandoPagamento,
        [StringValue("Pedido cancelado")]PedidoCancelado,
        [StringValue("Pagamento aprovado")]PagamentoAprovado,
        [StringValue("Pagamento cancelado")]PagamentoCancelado,
        [StringValue("Enviado para transportadora")]EnviadoParaTransportadora,
        [StringValue("Entregue")]Entregue
    }
}
