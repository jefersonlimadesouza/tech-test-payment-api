
using System.Net;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ListaProdutosController : ControllerBase
    {
        private ProdutoContext _context;

        public ListaProdutosController(ProdutoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(ListaProdutos listaProdutos)
        {
            _context.Add (listaProdutos);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId),
            new { id = listaProdutos.Id },
            listaProdutos);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var listaProdutos = _context.ListaProdutos.Find(id);
            if (listaProdutos == null) return NotFound();

            return Ok(listaProdutos);
        }

        [HttpGet("ObterPorLista")]
        public IActionResult ObterPorLista(int lista)
        {
            var nomeListas =
                _context
                    .ListaProdutos
                    .Where(x => x.IdListaProdutos == lista);

            if (nomeListas == null) return NotFound();

            return Ok(nomeListas);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, ListaProdutos listaProdutos)
        {
            var listaProdutosBanco = _context.ListaProdutos.Find(id);
            if (listaProdutosBanco == null) return NotFound();

            listaProdutosBanco.IdListaProdutos = listaProdutos.IdListaProdutos;
            listaProdutosBanco.IdProduto = listaProdutos.IdProduto;

            _context.ListaProdutos.Update (listaProdutosBanco);
            _context.SaveChanges();

            return Ok(listaProdutosBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var listaProdutosBanco = _context.ListaProdutos.Find(id);
            if (listaProdutosBanco == null) return NotFound();

            _context.ListaProdutos.Remove (listaProdutosBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
