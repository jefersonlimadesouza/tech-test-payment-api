using System.Net;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private ProdutoContext _context;

        public ProdutoController(ProdutoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Produto produto)
        {
            _context.Add (produto);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId),
            new { id = produto.Id },
            produto);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var produto = _context.Produtos.Find(id);
            if (produto == null) return NotFound();

            return Ok(produto);
        }

        [HttpGet("ObterPorNomeProduto")]
        public IActionResult ObterPorNome(string nomeProduto)
        {
            var nomeProdutos = _context.Produtos.Where(x => x.NomeProduto.Contains(nomeProduto));

            if (nomeProdutos == null) return NotFound();

            return Ok(nomeProdutos);
        }
        
        [HttpGet("ObterPorDescricaoProduto")]
        public IActionResult ObterPorDescrecao(string nomeProduto)
        {
            var nomeProdutos = _context.Produtos.Where(x => x.DescricaoProduto.Contains(nomeProduto));

            if (nomeProdutos == null) return NotFound();

            return Ok(nomeProdutos);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Produto produto)
        {
            var produtoBanco = _context.Produtos.Find(id);
            if (produtoBanco == null) return NotFound();

            produtoBanco.NomeProduto = produto.NomeProduto;
            produtoBanco.DescricaoProduto = produto.DescricaoProduto;
            produtoBanco.Preco = produto.Preco;
            produtoBanco.Saldo = produto.Saldo;


            _context.Produtos.Update (produtoBanco);
            _context.SaveChanges();

            return Ok(produtoBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var produtoBanco = _context.Produtos.Find(id);
            if (produtoBanco == null) return NotFound();

            _context.Produtos.Remove (produtoBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
