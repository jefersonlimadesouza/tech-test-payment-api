using System.Net;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private ProdutoContext _context;

        public VendedorController(ProdutoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {
            _context.Add (vendedor);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId),
            new { id = vendedor.Id },
            vendedor);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.Vendedor.Find(id);
            if (vendedor == null) return NotFound();

            return Ok(vendedor);
        }

        [HttpGet("ObterPorNomeVendedor")]
        public IActionResult ObterPorNome(string nomeVendedor)
        {
            var nomeVendedores =
                _context
                    .Vendedor
                    .Where(x => x.NomeVendedor.Contains(nomeVendedor));

            if (nomeVendedores == null) return NotFound();

            return Ok(nomeVendedores);
        }

        [HttpGet("ObterPorCPF")]
        public IActionResult ObterPorDescrecao(string cpf)
        {
            var vendedores = _context.Vendedor.Where(x => x.CPF.Contains(cpf));

            if (vendedores == null) return NotFound();

            return Ok(vendedores);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedor.Find(id);
            if (vendedorBanco == null) return NotFound();

            vendedorBanco.CPF = vendedor.CPF;
            vendedorBanco.NomeVendedor = vendedor.NomeVendedor;
            vendedorBanco.EmailVendedor = vendedor.EmailVendedor;
            vendedorBanco.TelefoneVendedor = vendedor.TelefoneVendedor;

            _context.Vendedor.Update (vendedorBanco);
            _context.SaveChanges();

            return Ok(vendedorBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorBanco = _context.Vendedor.Find(id);
            if (vendedorBanco == null) return NotFound();

            _context.Vendedor.Remove (vendedorBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
