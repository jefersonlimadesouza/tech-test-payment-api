using System.Net;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private ProdutoContext _context;

        public VendaController(ProdutoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            _context.Add (venda);

            // _context.Vendas.Status = "Aguardando pagamento";
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId),
            new { id = venda.Id },
            venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null) return NotFound();

            return Ok(venda);
        }

        [HttpPut("{id}/{statusVenda}")]
        public IActionResult
        AtualizarStatusVenda(
            int id,
            EnumStatusVenda statusVenda
        )
        {
            var vendaBanco = _context.Vendas.Find(id);
            if (vendaBanco == null) return NotFound();

            if (((int)statusVenda) == 0)
            {
                vendaBanco.Status = "Aguardando pagamento";
            }
            else if (((int)statusVenda) == 1)
            {
                vendaBanco.Status = "Pedido cancelado";
            }
            else if (((int)statusVenda) == 2)
            {
                vendaBanco.Status = "Pagamento aprovado";
            }
            else if (((int)statusVenda) == 3)
            {
                vendaBanco.Status = "Pagamento cancelado";
            }
            else if (((int)statusVenda) == 4)
            {
                vendaBanco.Status = "Enviado para transportadora";
            }
            else if (((int)statusVenda) == 5)
            {
                vendaBanco.Status = "Entregue";
            }

            _context.Vendas.Update (vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }
    }
}
